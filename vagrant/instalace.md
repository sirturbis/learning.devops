# Postup instalace

testováno na Ubuntu 20.04

## obecný postup

1. v BIOSu povolit virtualizaci
2. nainstalovat virtualbox/kvm/wmvare
3. nainstalovat vagrant

## Povolení virtualizace v BIOSu

* nabootovat do BIOSu
  * F2 při startu
  * prerusit klasivké bootování a vybrat BIOS
* ve složce `Security` vybral `Virtualization` a přepnou na `Enable`

## Instalace VirtualBoxu

* zadat následující příkazy do terminálu

```bash
wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] http://download.virtualbox.org/virtualbox/debian $(lsb_release -cs) contrib"
sudo apt update
sudo apt-get install virtualbox-6.1
```

* stáhnout a nainstalovat VirtualBox [Extension Pack](https://download.virtualbox.org/virtualbox/6.1.10/Oracle_VM_VirtualBox_Extension_Pack-6.1.10.vbox-extpack)
  * umožní využívat USB2/3, RDP,...
  * intalace proběhne spuštěním staženého souboru (jako u Widlí)

## Instalace KVM

##TODO

## Instalace VMware

##TODO

## Instalace Vagrantu

* zadat `sudo apt install vagrant`

