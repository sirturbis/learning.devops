# První spuštění Vagrantu

* vytvořit adresář, ve kterém se budou ukládat data související s Vagrantem
  * např. `mkdir ~/vagrant_project`
* první inicializace, která vytvoří vše potřebné, zejména soubor `Vagrantfile` (lze provést i jinak)
  * `vagrant init ubuntu/bionic64` 

## Nejzákladnější příkazy

* `vagrant up` - vytvorí vm nadefinována ve Vagrantfile
* `vagrant halt` - stopně vm
* `vagrant destroy` - zruší vm
* `vagrant ssh [name]` - přes SSH se připojí do VM, pokud je jich více, tak se bud musí nadefinovat defaultní anebo se musí zadat i jméno vm, do kterého se chci připojit
* `vagrant provision` - spustí dopsané provision bez toho aby bylo nutné vm ukoncit a spustit znovu
