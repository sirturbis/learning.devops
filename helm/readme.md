# HELM

## Installation
* brew install kubernetes-helm - install client part
* helm repo add stable https://kubernetes-charts.storage.googleapis.com/


## Usefull packages

### Prometheus
* `helm inspect values stable/prometheus > /tmp/prometheus.values`
* `vi /tmp/prometheus.values` -> set loadBalancerIP to 20.20.20.100 and enable storageclass and set it to `nfs-storageclass`
* `kubectl create namespace prometheus`
* `helm install prometheus stable/prometheus --values /tmp/prometheus.values --namespace prometheus`
* `export POD_NAME=$(kubectl get pods --namespace prometheus -l "app=prometheus,component=server" -o jsonpath="{.items[0].metadata.name}")`
* `kubectl --namespace prometheus port-forward $POD_NAME 9090`

### Grafana
* `helm inspect values stable/grafana > /tmp/grafana.values`
* `vi /tmp/grafana.values` -> set loadBalancerIP to 20.20.20.100 and enable storageclass and set it to `nfs-storageclass`
* edit admin credencials too `# adminUser: admin adminPassword: myadminpassword`
* disable initchowndata `enabled: false`
* `kubectl create namespace grafana`
* add plugin for pie chart `grafana-piechart-panel` into  plugin section in values.yaml
* `helm install grafana stable/grafana --values /tmp/grafana.values --namespace grafana`
* `export POD_NAME=$(kubectl get pods --namespace grafana -o jsonpath="{.items[0].metadata.name}")`
* `kubectl --namespace grafana port-forward $POD_NAME 3000`
* set prometheus datasource `http://prometheus-server.prometheus.svc.cluster.local`
* deploy ansible role kubernetes.app.grafana

### Loki
* `helm repo add loki https://grafana.github.io/loki/charts`
* `helm repo update`
* `kubectl create namespace loki`
* `helm install loki --namespace=loki loki/loki-stack`
* add datasource to grafana as `http://loki.loki.svc.cluster.local`
