# LEARNING DEVOPS

## Kubernetes

### Installation

* 3 working nodes (1 master, 2 workers)
* cd ansible && ansible-playbook -i hosts create_cluster.yml

### Usefull links
* [Rook - cloud native filesystem](https://rook.io/docs/rook/v1.3/ceph-quickstart.html)

### Usefull commands

* kubectl get pods --all-namespaces - print all pods in all namespaces
* kubectl get pods -n $namespace    - print all pods in namespace with name $namespaces
* kubectl proxy -->  your  [kubernetes dashboards](http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/)