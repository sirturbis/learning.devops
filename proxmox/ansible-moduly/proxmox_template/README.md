# Ansible modul - proxmox_template

* [ansible documentation](https://docs.ansible.com/ansible/latest/modules/proxmox_template_module.html)

## Popis

* Umožňuje práci s templaty v Proxmox VE clusteru

## SW požadavky

* [proxmoxer](https://pypi.org/project/proxmoxer/)
* [python >= 2.7](https://docs.python-guide.org/starting/install3/linux/)
* [request](https://pypi.org/project/requests/)
* [ansible >= 2.9](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

### Doporučení

* nainstalovat python 2.7 a 3.6
* nainstalovat pip a pip3
