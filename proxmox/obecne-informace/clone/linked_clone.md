# obecné informace

* slouží k naklonování VM z VM anebo VM z templaty
* doba klonování nezávisí na velikosti disku klonovené předlohy
* Naklonované VM je až na VMID schodná s předlohou
* Naklonované VM JE závislé na předloze a bez ní není funkční
* proces klonování je skoro instantní
* pomocí ansible může probíhat pouze 1 klonování současně
* pomocí WEB GUI může probíhat neomezene klonování současně
* nelze klonovat i na jinu storage nez je klonovaný vzor
* nelze klonovat na jakékoliv uložiště
  * uložistě NESMÍ být typu BLOCK (LVM, LVM-thin, ...
  * uložistě MUSÍ vytu typu FILE (Directory, ZFS, NFS, ...)