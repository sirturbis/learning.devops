# Obecné informace

* slouží k naklonování VM z VM anebo VM z templaty
* doba klonování závisí na velikosti disku clonovené předlohy
* Naklonované VM je až na VMID schodná s předlohou
* Naklonované VM NENÍ závislé na předloze
* pomocí ansible může probíhat pouze 1 klonování současně
* pomocí WEB GUI může probíhat neomezene klonování současně, při větším množství klonování se ale proces výrazne zpomaluje.
* lze klonovat i na jinu storage nez je klonovaný vzor
* lze klonovat na jakékoliv uložiště