# Obecné informace

* cloud init není defaultně vytvořen a tudíž při tvorbě VM se pro něj nevytvoří storage
* NEMUSÍ být ve stejném storage jako VM, či template
* lze pomocí něj definovat:
  * IPV4 + gateway
  * IPW6 + gateway
  * jméno a hslo uživatele
  * ssh klíč uživatele
  * proxy # TODO zkontrolovat, zda nekecam

## Využití - TEMPLATE

* v terminálu se vytvoří nove VM
* z internetu se stíhne image, obsahující už nadefinovaný cloud-init či se takový image vytvoří
  * definuje se uživatel, heslo, nainstalované programy, ...
* do VM se přidá storage a mounte se do něj image obsahující cloud-init
* z VM se vygeneruje templata, která se používá pro klonování VM

## Využití - VM

* normálním postupem se vytvoří VM, včetně OM a koncové instalace
* pripojíme se na VM a doinstalujeme cloud-init
  * pro debian-like: `sudo apt-get install cloud-init -y`
* odhlásit se z VM
* vytvořit na proxmox serveru uložiště pro cloud-init
  * `qm set VMID --ide2 STORAGE:cloudinit`
* v WEB GUI vybereme VM, ke které jsme vytvořili storage pro cloud-init --> hardware --> storage --> ADD --> cloud-init
* v tento okamžik můžeme nastavovat cloud-init
* doporučuji takto nastavená VM convertovat do template // TODO jak na convert // a používat ke klonování