# Postup vytvoření storage typu Direction

* Vytváří se na prázdné disk partition

## Vytvoření ext4 filesystem na vybrané partitioně a přiřadíme volume label

* `mkfs.ext4 -L NAZEV_VOLUME_LABELU /dev/sdXY`
  * X je označení dísku (a, b, c, ...)
  * Y je číslo partitiony na disku (1, 2, 3, ...)
    * příklad sbd2 - nadále využíváno v návodu
  * za NAZEV_VOLUME_LABELU můžeme dosadit jakýkoliv název, který není využíván
    * přiklad storage - nadále využíváno v návodu

## mountnutí disku ke složce

* může sepoužit jakákoliv složka
* `mkdir -p /MOUNTNUTA/SLOZKA`
  * pravidlem využívat slozku `/mnt/data`
* zeditujeme soubor /etc/fstab
  * `$EDITOR /etc/fstab`
  * na konec přidáme řádek:  `LABEL=NAZEV_VOLUME_LABELU MOUNTNUTA_SLOZKA ext4 defaults 0 2`
    příklad `LABEL=storage /mnt/data ext4 defaults 0 2`
* mount -a
  * mountne všechny složky, které mají mountnutí nastavené

## Využití storage 

* Pro využítí storage na více učelů, je vhodné pro každá učel vytvořit složku, do které se budou data ukládat
* pro backup
  * `mkdir -p /mnt/data/backup`
* pro VM
  * `mkdir -p /mnt/data/vm`

## Vytvoření storage v WEB GUI Proxmoxu

* Datacenter --> Storage --> Add --> zvolit "Directory"
* ID - název storage, pod kterým bude působit v proxmoxu
  * napr. backup
* Directory - zvolíme cestu do složky, dle využítí
  * např. /mnt/data/backup
* Content - kliknutím vybereme co může být v storage ukládáno