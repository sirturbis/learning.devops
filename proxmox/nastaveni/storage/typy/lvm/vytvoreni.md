# Postup vytvoření storage typu LVM

* Vytváří se na prázdné disk partition

## Vytvoření physical volume

* `pvcreate /dev/sdXY`
  * X je označení dísku (a, b, c, ...)
  * Y je číslo partitiony na disku (1, 2, 3, ...)
  * příklad sbd1 - nadále využíváno v návodu

## Vytvoření volume group

* `vgcreate NAZEV_VOLUME_GRUPY /dev/sbd1`
  * za NAZEV_VOLUME_GRUPY můžeme dosadit jakýkoliv název, který není využíván
  * příklad vmdata - nadále využíváno v návodu

## Vytvoření storage v WEB GUI Proxmoxu

* Datacenter --> Storage --> Add --> zvolit "LVM"
* ID - název storage, pod kterým bude působit v proxmoxu
  * napr. kubernetes
* Base storage - Existing volume groups
* Volume group - vmdata
* Content - kliknutím vybereme co může být v storage ukládáno