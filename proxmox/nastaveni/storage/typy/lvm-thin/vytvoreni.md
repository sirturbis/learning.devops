# Postup vytvoření storage typu LVM-thin

* Vytváří se na prázdném místě LVM storage, definované pomocí volume group

## Vytvoření thin pool na LVM

* `lvcreate -L VELIKOST -n NAZEV_THIN_POOL NAZEV_VOLUME_GROUP`
  * za VELIKOST se dosazuje číslo a jendotky
    * příklad 100G
  * za NAZEV_THIN_POOL můžeme dosadit jakýkoliv název, který není využíván
    * příklad vmtest - nadále využíváno v návodu
  * NAZEV_VOLUME_GROUP - existující volume group, na které byl vytvořen LVM
* `lvconvert --type thin-pool NAZEV_VOLUME_GROUP/NAZEV_THIN_POOL`

## Vytvoření storage v WEB GUI Proxmoxu

* Datacenter --> Storage --> Add --> zvolit "LVM-THIN"
* ID - název storage, pod kterým bude působit v proxmoxu
  * napr. kubernetes
* //TODO dopsat
* //TODO dopsat
* Content - kliknutím vybereme co může být v storage ukládáno