# Disk partiton

* Při přidání nových HW disků je potřeba vytvorit disk partiton/y
* disk partitiony upravujeme i v případě, že chceme na daném disku upravit storage

## Postup

* pomocí `lsblk` identifikujeme označení nového disku.
  * formát je sbX, kde X je písmeno určující disk
* pomocí `fdisk /dev/sdX` vyvoláme program pro práci s partitonem
  * práce s programem je jednoduchá - zmáčknout `m` pro nápovědu a řídit se pokyny
  * pro vytvoření nové partitiony zvolíme `n` a následně vybereme odkud kam (velikost) bude partitiona zabírat místo na disku
  * pro smazání zvolíme `d`
  * pro uložení `w`
* po smazání paritiony je nutné provést restart a teprve poté vytvořit partitony nové. V případě, že se tak neučíní, si bude systém pamatovat starou parititonu a to až do restartu, práce bude matoucí a bude vypisovat realtivně nesrozumitelné chybové hlášky.  