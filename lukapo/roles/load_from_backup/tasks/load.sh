. /root/.duply/s3/credentials
duply s3 list | grep $1
duply s3 fetch var/www/sites/mio_wh_$1 /tmp/mio_wh_$1 "'$2'"
duply s3 list | grep $1 | grep sql
duply s3 fetch mysql-backup-duply/wh$1.sql.xz /tmp/wh$1.sql.xz 
xz -d wh$1.sql.xz
