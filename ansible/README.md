# How things work

## PLAYBOOK

### change_vm_specv.yml

* EPIC TASK
* Slouží ke změně HW spec na VM

### new_vm_specs.yml

* SUBTASK
* Nastavuje nové HW spec na VM

### vm_state_started.yml

* SUBTASK
* Spouští VM

### vm_state_stoped.yml

* SUBTASK
* Zastavuje VM

### delete_vm.yml

* SUBTASK
* maže VM

### add_user_and_shh_to_vm.yml

* TASK
* na servery přidá uživatele a přiřadím jim jejich SSH klíče a heslo

### create_cluster

* TASK
* na serverech nainstaluje python, kubernetes, atd.
* nastaví roli mastera a workerů

### create_vm

* EPIC TASK
* vytvoří VM dle nastavení z templatu
* pridá nové uživatele, jejich hesla a SSH klče
* na VM nastavý předsnastavené IPV4 a gatewaye

### create_kubernetes

* MEGA EPIC TASK
* zkombinuje create_vm a create_cluster

## ROLE

### proxmox

* Slouží k vytváření VM na serveru pomocí Proxmoxu.
* VM jsou vytvářené z templatů a nastavené dle předchozích požadavků a upravovány pomocí cloud-init.

### proxmox/add-user

* Slouží k vytvoření nových uživatelů na serverech a přiřazení jejich public ssh klíče.
* data čerpá z:
  * group_vars/kubernetes/vars.yml z proměnné vm_user

### proxmox/clone_vm_from_template

* Slouží k vytvoření VM, které jsou nastaveny dle požadavků
* data čerpá z:
  * group_vars/proxmox_server/vars.yml z proměnné vm_info
  * proxmox/worker/defaults/main.yml

### proxmox/state

* Slouží k nastavení state VM
* data čerpá z:
  * group_vars/proxmox_server/vars.yml z proměnné vm_info
  * proxmox/state/defaults/main.yml

### proxmox/ip-change

* Slouží k nastavení požadované IPV4 adresy a gatewaye na nově vytvořených VM
* data čerpá z:
  * group_vars/proxmox_server/vars.yml z proměnné vm_info

### proxmox/specs_change

* Slouží k předefinování počtu CPU jader a množsví RAM
* data čerpá z
  * group_vars/proxmox_server/vars.yml z proměnné vm_info
  * proxmox/specs_change/defaults/main.yml

### proxmox/disc_resize

* Slouží k k nastavení velikosti disku
* data čerpá z
  * group_vars/proxmox_server/vars.yml z proměnné vm_info
  * proxmox/specs_change/defaults/main.yml

## VARS

### group_vars/proxmox_server/vars

* Slouží k zadání vstupních parametrů a nastavení, které má nová VM obsahovat
* vm_info - zadávání vstupních dat pro tvoření templaty
  * id[integer] - požadvané VMID
  * name[integer] - požadnované jméno VM
  * ipv4[string] - pozadovaní IPV4 adresa
  * gw[string] - požadovaná gateway
  * cores[integer] - počet jader
  * memory[integer] - počet RAM v MB
  * disc_type[string] - typ disku
  * disc_size[integer] - požadovaná velikost disku

### group_vars/kuberenetes/vars

* Slouží k zadnání vstupních parametrů pro vytvoření a nastavení přístupu novým uživatelům na VM
* vm_user - zadávání infomrací k tvorbě nových uživatelů
  * name[string] - jméno uživatele
  * password[string] - v hashi zakodované heslo - defaultně je to "x"
  * ssh[string] - public ssh klíč uživatele

### proxmox/master/defaults/main.yml

* Slouží k nastavení vstupních parametrů při tvorbe VM - worker
* template_master[string] - výběru templaty, který slouží k vytvážení VM - master
* vm_storage[integer] - výběr uložistě pro VM - varianty: stable / learning
* timeout_time[integer] - čas, po který ansible čeká na vytvoření VM

### proxmox/worker/defaults/main.yml

* Slouží k nastavení vstupních parametrů při tvorbe VM - master
* template_worker[string] - výběru templaty, který slouží k vytvážení VM - workera
* vm_storage[integer] - výběr uložistě pro VM - varianty: stable / learning
* timeout_time[integer] - čas, po který ansible čeká na vytvoření VM

### proxmox/state/defaults/main.yml

* timeout_time[integer] - čas, po který se čeká na nastavení state

### proxmox/specs_change/defaults/main.yml

* timeout_time[integer] - čas, po který se čeká na nastavení nových spec. paramertů
