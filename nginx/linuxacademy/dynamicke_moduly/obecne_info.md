# Obecné informace

* Dynamické moduly slouží k přidávání funkcí bez nutnosti compilovat cely nginx - stačí pouze překompilovat modul
* nacházejí se v /etc/nginx/modules/
* moduly defaultně nainstalované (a nedynamické) lze zjistit pomocí příkazu `nginx -V 2>&1 | tr -- - '\n' | grep _module`