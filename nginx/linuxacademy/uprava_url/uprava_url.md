# Generating Self-signed Certifikates

## využité moduly

* [try files](http://nginx.org/en/docs/http/ngx_http_core_module.html#try_files)
* [rewrite](http://nginx.org/en/docs/http/ngx_http_rewrite_module.html#rewrite)

## generovní certifikátu

```bash
server {
    listen 80 default_server;
    listen 443 ssl;
    server_name _;
    root /usr/share/nginx/html;

    ssl_certificate /etc/nginx/ssl/public.pem;
    ssl_certificate_key /etc/nginx/ssl/private.key;

    rewrite ^(/.*)\.html(\?.*)?$ $1$2 redirect;
    rewrite ^/(.*)/$ /$1 redirect;

    location / {
        try_files $uri/index.html $uri.html $uri/ $uri =404;
    }

    location = /admin {
        auth_basic "Login Required";
        auth_basic_user_file /etc/nginx/.htpasswd;
        try_files $uri/index.html $uri.html $uri/ $uri =404;
    }

    error_page 404 /404.html;
    error_page 500 501 502 503 504 /50x.html;
}
```  

### dulezité body

* try_files $uri/index.html $uri.html $uri/ $uri =404; - zkousi najit ruzne variace url, pokud nenajde posle 404
* rewrite ^(/.*)\.html(\?.*)?$ $1$2 redirect; - odstranuje `.html` z odkazu
* rewrite ^/(.*)/$ /$1 redirect; - odstranuje `/` z odkazu

## vysvetlivky

```text
Let’s break down our first regex: ^(/.*)\.html(\?.*)?$. The starting ^ indicates that we’re going from the very beginning of the URI. We create a capture group using parentheses. The pattern /.* means starting with a / and including all characters after that. Next, we expect to find a .html (notices that we needed to escape it using a ). Lastly, we create another capture group that gets an optional query string (anything after a ?) until the end of the URI (as indicated by the $ character). The capture groups will be usable in the REPLACEMENT portion of the direction using $1, $2, etc.
```