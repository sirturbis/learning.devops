# Redirect from http to https

```code
server {
    listen 80 default_server;
    server_name _;
    return 301 https://$host$request_uri;
}
```

* přidá se druhý server, ve kterém se použije directive `return` a proměnné `$request_uri` a `$host`

### dulezité body

* `return 301 https://$host$request_uri;` - 301 status code. As for the URL portion of the statement, we’re going to use variables that are present when processing the request to ensure that we go to the proper spot. To ensure that it goes to the proper server we’ll use the $host variable in our destination URL to pass along the domain name. Using $host will make this server redirect to the proper server after we have multiple HTTPS, virtual hosts. We’ll also use the $request_uri variable which represents the path portion of the request.