# Generating Self-signed Certifikates

## využité moduly

* ## TODO DOPSAT

## generovní certifikátu

```bash
[root] $ mkdir /etc/nginx/ssl
[root] $ openssl req -x509 -nodes -days 365 \
  -newkey rsa:2048 \
  -keyout /etc/nginx/ssl/private.key \
  -out /etc/nginx/ssl/public.pem
```  

## Vysvětlivky 

req - We’re making a certificate request to OpenSSL

-x509 - Specifying the structure that our certificate should have. Conforms to the X.509 standard

-nodes - Do not encrypt the output key

-days 365 - Set the key to be valid for 365 days

-newkey rsa:2048 - Generate an RSA key that is 2048 bits in size

-keyout /etc/nginx/ssl/private.key - File to write the private key to

-out /etc/nginx/ssl/public.pem - Output file for public portion of key

## Nastavení použití certifikátu

```bash
server {
    listen 80 default_server;
    listen 443 ssl;
    server_name _;
    root /usr/share/nginx/html;

    ssl_certificate /etc/nginx/ssl/public.pem;
    ssl_certificate_key /etc/nginx/ssl/private.key;

    location = /admin.html {
        auth_basic "Login Required";
        auth_basic_user_file /etc/nginx/.htpasswd;
    }

    error_page 404 /404.html;
    error_page 500 501 502 503 504 /50x.html;
}
```

### dulezité body

* listen 443 ssl; - na kterém portu "naslouchá" SSL
* ssl_certificate /etc/nginx/ssl/public.pem; - umístění certifikátu
* ssl_certificate_key /etc/nginx/ssl/private.key; - umístení klíce k certifikátu